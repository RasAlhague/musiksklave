﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Lavalink;
using DSharpPlus.Net;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using MusicBotLib.Services;
using MusicBotLib.Commands;

namespace MusikSklave
{
    class Program
    {
         static void Main(string[] args)
        {
            MainAsync().GetAwaiter().GetResult();
        }

        static async Task MainAsync()
        {
            Configuration config = Configuration.GetInstance();

            var discord = new DiscordClient(new DiscordConfiguration()
            {
                Token = config.BotToken,
                TokenType = TokenType.Bot,
                MinimumLogLevel = config.LogLevel
            });
            ConfigureBot(config, discord);
            LavalinkConfiguration lavalinkConfig;
            LavalinkExtension lavalink;
            ConfigureLavalink(config, discord, out lavalinkConfig, out lavalink);

            await discord.ConnectAsync();
            await lavalink.ConnectAsync(lavalinkConfig);
            await Task.Delay(-1);
        }

        private static void ConfigureLavalink(Configuration config, DiscordClient discord, out LavalinkConfiguration lavalinkConfig, out LavalinkExtension lavalink)
        {
            var endpoint = new ConnectionEndpoint
            {
                Hostname = config.LavalinkAddress,
                Port = config.LavalinkPort
            };

            lavalinkConfig = new LavalinkConfiguration
            {
                Password = config.LavalinkPasswort,
                RestEndpoint = endpoint,
                SocketEndpoint = endpoint
            };
            lavalink = discord.UseLavalink();
        }

        private static void ConfigureBot(Configuration config, DiscordClient discord)
        {
            var services = new ServiceCollection()
                            .AddSingleton<IMusicService, MusicService>()
                            .BuildServiceProvider();

            var commands = discord.UseCommandsNext(new CommandsNextConfiguration
            {
                StringPrefixes = new string[] { config.Prefix },
                Services = services
            });
            commands.RegisterCommands<MusicCommandModule>();
        }
    }
}
