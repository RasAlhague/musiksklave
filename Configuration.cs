﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusikSklave
{
    class Configuration
    {
        private static Configuration _instance;

        public string BotToken { get; set; }
        public string Prefix { get; set; }
        public string LavalinkPasswort { get; set; }
        public string LavalinkAddress { get; set; }
        public LogLevel LogLevel { get; set; }
        public short LavalinkPort { get; set; }

        protected Configuration()
        {
        }

        public static Configuration GetInstance()
        {
            if (_instance != null)
            {
                return _instance;
            }

            var args = Environment.GetEnvironmentVariables();
            var configuration = new Configuration();

            configuration.BotToken = args["MUSIC_BOT_TOKEN"].ToString();
            configuration.Prefix = args["MUSIC_BOT_PREFIX"].ToString();
            configuration.LavalinkPasswort = args["LAVALINK_PASSWORT"].ToString();
            configuration.LavalinkAddress = args["LAVALINK_ADDRESS"].ToString();
            configuration.LogLevel = Enum.Parse<LogLevel>(args["MUSIC_BOT_LOGLEVEL"].ToString());
            configuration.LavalinkPort = short.Parse(args["LAVALINK_PORT"].ToString());

            _instance = configuration;

            return _instance;
        }
    }
}
